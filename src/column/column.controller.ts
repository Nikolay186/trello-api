import {
    Controller,
    Get,
    Put,
    Delete,
    UseGuards,
    Param,
    Post,
    Body,
    UsePipes,
    Query,
} from '@nestjs/common';  
import { User } from '../user/user.decorator';
import { AuthGuard } from '../share/auth.guard';
import { ValidationPipe } from '../share/validation.pipe';
import { ColumnDTO } from './column.dto';
import { ColumnService } from './column.service';
import { ApiTags, ApiCreatedResponse, ApiNotFoundResponse, ApiForbiddenResponse, ApiAcceptedResponse } from '@nestjs/swagger';

@ApiTags('Column controller')
@Controller('api/columns')
export class ColumnController
{
    constructor(private columnService: ColumnService) { }
    
    @Post('user/:id')
    @ApiCreatedResponse({description: 'Column created'})
    @ApiForbiddenResponse({description: 'Cannot create column'})
    @UseGuards(new AuthGuard())
    @UsePipes(new ValidationPipe())
    addColumn(@User('id') user: string, @Body() data: ColumnDTO)
    {
        return this.columnService.add(user, data);
    }
    
    @Get('user/:id')
    @ApiAcceptedResponse({description: 'Request accepted'})
    @ApiNotFoundResponse({description: 'User not found'})
    getUserColumns(@Param('id') user: string, @Query('page') page: number)
    {
        return this.columnService.getUserColumns(user, page);
    }
    
    @Get(':id')
    @ApiAcceptedResponse({description: 'Request accepted'})
    @ApiNotFoundResponse({description: 'Column not found'})
    getColumn(@Param('id') id: string)
    {
        return this.columnService.getColumn(id);
    }
    
    @Delete(':id')
    @ApiCreatedResponse({description: 'Column deleted'})
    @ApiNotFoundResponse({description: 'Column not found'})
    @ApiForbiddenResponse({description: 'Cannot delete column'})
    @UseGuards(new AuthGuard())
    removeColumn(@Param('id') id: string, @User('id') user: string)
    {
        return this.columnService.remove(id, user);
    }
}  