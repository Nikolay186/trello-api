import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../user/user.entity';
import { CardService } from '../card/card.service';
import { CardEntity } from '../card/card.entity';
import { ColumnController } from './column.controller';
import { ColumnService } from './column.service';
import { ColumnEntity } from './column.entity';
import { ColumnResolver } from './column.resolver';

@Module({
  imports: [TypeOrmModule.forFeature([ColumnEntity, CardEntity, UserEntity])],
  controllers: [ColumnController],
  providers: [ColumnService, ColumnResolver, CardService],
})
export class ColumnModule { }