import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ColumnEntity } from './column.entity';
import { UserEntity } from '../user/user.entity';
import { ColumnDTO } from './column.dto';

@Injectable()
export class ColumnService {
  constructor(
    @InjectRepository(ColumnEntity)
    private columnRepo: Repository<ColumnEntity>,
    @InjectRepository(UserEntity)
    private userRepo: Repository<UserEntity>,
  ) { }

  private toResponseObject(column: ColumnEntity) {
    return {
      ...column,
      author: column.owner && column.owner.toResponseObject(),
    };
  }

  private checkOwnership(column: ColumnEntity, userId: string) {
    if (column.owner.id !== userId) {
      throw new HttpException('Incorrect User', HttpStatus.UNAUTHORIZED);
    }
  }
  
  async getUserColumns(userId: string, page: number = 1)
  {
      const columns = await this.columnRepo.find({
          where: {owner: {id: userId}},
          relations: ['owner'],
          take: 25,
          skip: 25 * (page - 1)
      });
      return columns.map(column => this.toResponseObject(column));
  }
  
  async getColumn(id: string)
  {
      const column = await this.columnRepo.findOne({
          where: {id},
          relations: ['owner', 'cards']
      });
      return this.toResponseObject(column);
  }
  
  async add(userId: string, data: ColumnDTO)
  {
      const user = await this.userRepo.findOne({where: {id: userId}});
      const column = await this.columnRepo.create({
          ...data,
          owner: user
      });
      await this.columnRepo.save(column);
      return this.toResponseObject(column);
  }
  
  async update(id: string, userId: string, data: Partial<ColumnDTO>): Promise<ColumnDTO>
  {
      let column = await this.columnRepo.findOne({
          where: {id},
          relations: ['owner']
      });
      if (!column)
      {
          throw new HttpException('Column not found', HttpStatus.NOT_FOUND);
      }
      this.checkOwnership(column, userId);
      await this.columnRepo.update({id}, data);
      column = await this.columnRepo.findOne({
          where: {id},
          relations: ['owner']
      });
      return this.toResponseObject(column);
  }
  
  async remove(id: string, userId: string)
  {
      const column = await this.columnRepo.findOne({
          where: {id},
          relations: ['owner']
      });
      if (!column)
      {
        throw new HttpException('Column not found', HttpStatus.NOT_FOUND);
      }
      this.checkOwnership(column, userId);
      await this.columnRepo.remove(column);
      return this.toResponseObject(column);
  }
}