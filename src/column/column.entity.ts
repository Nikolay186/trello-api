import {
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    Column,
    ManyToOne,
    JoinTable,
    OneToMany
  } from 'typeorm';
  
import { UserEntity } from '../user/user.entity';
import { CardEntity } from '../card/card.entity';

@Entity('column')
export class ColumnEntity
{
    @PrimaryGeneratedColumn('uuid')
    id: string;
    
    @Column('text')
    name: string;
    
    @ManyToOne(type => UserEntity)
    @JoinTable()
    owner: UserEntity;
    
    @OneToMany(type => CardEntity, card => card.column, {cascade: true})
    cards: CardEntity[];
}