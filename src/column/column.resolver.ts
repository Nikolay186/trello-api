import { Resolver, Args, Query, Mutation, Context } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { ColumnService } from './column.service';
import { AuthGuard } from '../share/auth.guard';

@Resolver('Column')
export class ColumnResolver
{
    constructor(private columnService: ColumnService) { }
    
    @Query()
    async columns(@Args('userId') userId: string, @Args('page') page: number)
    {
        return await this.columnService.getUserColumns(userId, page);
    }
    
    @Query()
    async column(@Args('id') id: string)
    {
        return await this.columnService.getColumn(id);
    }
    
    @Mutation()
    @UseGuards(new AuthGuard())
    async addColumn(@Args('name') name: string, @Context('user') user)
    {
        const { id: userId } = user;
        const data = { name };
        return await this.columnService.add(userId, data);
    }
    
    @Mutation()
    @UseGuards(new AuthGuard())
    async removeColumn(@Args('id') id: string, @Context('user') user)
    {
        const { id: userId } = user;
        return await this.columnService.remove(id, userId);
    }
}