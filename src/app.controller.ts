import { Controller, Get } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Default')
@Controller()
export class AppController {
  @Get()
  @ApiResponse({status: 200, description: 'App is working now.'})
  index() {
    return { index: 'working' };
  }
}
