import {
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    Column,
    ManyToOne,
    JoinTable,
    OneToMany
  } from 'typeorm';
import { UserEntity } from 'src/user/user.entity';
import { ColumnEntity } from '../column/column.entity';
import { CommentEntity } from '../comment/comment.entity';

@Entity('card')
export class CardEntity
{
    @PrimaryGeneratedColumn('uuid')
    id: string;
    
    @Column('text')
    name: string;
    
    @Column('text', { nullable: true })
    content: string;
    
    @ManyToOne(type => UserEntity)
    @JoinTable()
    owner: UserEntity;
    
    @ManyToOne(type => ColumnEntity, col => col.cards, { onDelete: 'CASCADE'})
    column: ColumnEntity;
    
    @OneToMany(type => CommentEntity, comm => comm.card, {cascade: true })
    comms: CommentEntity[];
}