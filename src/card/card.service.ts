import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ColumnEntity } from '../column/column.entity';
import { UserEntity } from '../user/user.entity';
import { CardEntity } from './card.entity';
import { CardDTO } from './card.dto';

@Injectable()
export class CardService
{
    constructor(
        @InjectRepository(CardEntity)
        private cardRepo: Repository<CardEntity>,
        @InjectRepository(ColumnEntity)
        private columnRepo: Repository<ColumnEntity>,
        @InjectRepository(UserEntity)
        private userRepo: Repository<UserEntity>
    ) { }
    
    private toResponseObject(card: CardEntity)
    {
        return {
            ...card,
            owner: card.owner && card.owner.toResponseObject(),
        };
    }
    
    private checkOwnership(card: CardEntity, userId: string)
    {
        if (card.owner.id !== userId)
        {
            throw new HttpException('Not an owner', HttpStatus.UNAUTHORIZED);
        }
    }
    
    async showCards(colId: string, page: number = 1) 
    {
        const cards = await this.cardRepo.find({
            where: {col: { id: colId }},
            relations: ['owner', 'column'],
            take: 25,
            skip: 25 * (page - 1),
        });
        return cards.map(card => this.toResponseObject(card));
    }
    
    async showOne(id: string)
    {
        const card = await this.cardRepo.findOne({
            where: { id },
            relations: ['owner', 'column']
        });
        return this.toResponseObject(card);
    }
    
    async add(colId: string, userId: string, data: CardDTO)
    {
        const column = await this.columnRepo.findOne({where: {id: colId}});
        const user = await this.userRepo.findOne({where: {id: userId}});
        const card = await this.cardRepo.create({
            ...data,
            column,
            owner: user,
        });
        await this.cardRepo.save(card);
        return this.toResponseObject(card);
    }
    
    async update( id: string, userId: string, data: Partial<CardDTO>): Promise<CardDTO>
    {
        let card = await this.cardRepo.findOne({
            where: { id },
            relations: ['owner', 'column']
        });
        if (!card)
        {
            throw new HttpException('Card not found', HttpStatus.NOT_FOUND);
        }
        
        this.checkOwnership(card, userId);
        await this.cardRepo.update({id}, data);
        card = await this.cardRepo.findOne({
            where: { id },
            relations: ['owner', 'column']
        });
        return this.toResponseObject(card);
    }
    
    async remove(id: string, userId: string)
    {
        const card = await this.cardRepo.findOne({
            where: { id },
            relations: ['owner', 'column']
        });
        
        this.checkOwnership(card, userId);
        await this.cardRepo.remove(card);
        return this.toResponseObject(card);
    }
}