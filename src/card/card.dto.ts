import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CardDTO 
{
    @ApiProperty()
    @IsString()
    readonly name: string;
    @ApiProperty()
    @IsString()
    readonly content: string;
}