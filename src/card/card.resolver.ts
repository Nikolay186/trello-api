import { Resolver, Args, Query, Mutation, Context } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';

import { CardService } from './card.service';
import { AuthGuard } from '../share/auth.guard';

@Resolver('Card')
export class CardResolver
{
    constructor(private cardService: CardService) { }
    
    @Query()
    async getCard(@Args('id') id: string)
    {
        return await this.cardService.showOne(id);
    }
    
    @Mutation()
    @UseGuards(new AuthGuard())
    async addCard(
        @Args('column') colId: string, @Args('name') name: string,
        @Args('content') content: string, @Context('user') user,) {
        const { id: userId } = user;
        const data = { name, content };
        return await this.cardService.add(colId, userId, data);
    }
    
    @Mutation()
    @UseGuards(new AuthGuard())
    async removeCard(@Args('id') id: string, @Context('user') user)
    {
        const { id: userId } = user;
        return await this.cardService.remove(id, userId);
    }
}