import {
    Controller,
    Get,
    Put,
    Delete,
    UseGuards,
    Param,
    Logger,
    Post,
    Body,
    UsePipes,
    Query,
  } from '@nestjs/common';
import { User } from '../user/user.decorator';
import { AuthGuard } from '../share/auth.guard';
import { ValidationPipe } from '../share/validation.pipe';
import { CardDTO } from './card.dto';
import { CardService } from './card.service';
import { ApiTags, ApiResponse, ApiAcceptedResponse, ApiCreatedResponse, ApiForbiddenResponse, ApiNotFoundResponse } from '@nestjs/swagger';

@ApiTags('Card controller')
@Controller('api/cards')
export class CardController
{
    constructor(private cardService: CardService) { }
    
    @Get('list/:id')
    @ApiAcceptedResponse({description: 'Request accepted'})
    showCardsInColumn(@Param('id') col: string, @Query('page') page: number)
    {
        return this.cardService.showCards(col, page);
    }
    
    @Post('list/:id')
    @ApiCreatedResponse({description: 'Card created'})
    @ApiForbiddenResponse({description: 'Cannot create card'})
    @UseGuards(new AuthGuard())
    @UsePipes(new ValidationPipe())
    addCardInColumn(@Param('id') col: string, @User('id') user: string, @Body() data: CardDTO)
    {
        return this.cardService.add(col, user, data);
    }
    
    @Put(':id')
    @ApiCreatedResponse({description: 'Card updated'})
    @ApiNotFoundResponse({description: 'Card not found'})
    @ApiForbiddenResponse({description: 'Cannot create card'})
    @UseGuards(new AuthGuard())
    @UsePipes(new ValidationPipe())
    updateCard(@Param('id') id: string, @User('id') user: string, @Body() data: CardDTO)
    {
        return this.cardService.update(id, user, data);
    }
    
    @Get(':id')
    @ApiAcceptedResponse({description: 'Request accepted'})
    @ApiNotFoundResponse({description: 'Card not found'})
    showCard(@Param('id') id: string)
    {
        return this.cardService.showOne(id);
    }
    
    @Delete(':id')
    @ApiCreatedResponse({description: 'Card deleted'})
    @ApiNotFoundResponse({description: 'Card not found'})
    @ApiForbiddenResponse({description: 'Cannot delete card'})
    @UseGuards(new AuthGuard())
    removeCard(@Param('id') id: string, @User('id') user: string)
    {
        return this.cardService.remove(id, user);
    }
}