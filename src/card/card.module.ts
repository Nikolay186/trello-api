import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { ColumnEntity } from "../column/column.entity";
import { UserEntity } from "src/user/user.entity";
import { CardController } from "./card.controller";
import { CardService } from "./card.service";
import { CardEntity } from "./card.entity";
import { CardResolver } from "./card.resolver";

@Module({
    imports: [TypeOrmModule.forFeature([CardEntity, ColumnEntity, UserEntity])],
    controllers: [CardController],
    providers: [CardService, CardResolver]
})
export class CardModule { }