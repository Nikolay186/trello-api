import { Resolver, Args, Query, Mutation, Context } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { CommentService } from './comment.service';
import { AuthGuard } from '../share/auth.guard';

@Resolver('Comment')
export class CommentResolver {
  constructor(private commentService: CommentService) { }

  @Query()
  async getComment(@Args('id') id: string) {
    return await this.commentService.get(id);
  }

  @Mutation()
  @UseGuards(new AuthGuard())
  async addComment(@Args('card') cardId: string, @Args('comment') comment: string, @Context('user') user,) 
  {
    const { id: userId } = user;
    const data = { comment };
    return await this.commentService.add(cardId, userId, data);
  }

  @Mutation()
  @UseGuards(new AuthGuard())
  async removeComment(@Args('id') id: string, @Context('user') user) {
    const { id: userId } = user;
    return await this.commentService.remove(id, userId);
  }
}