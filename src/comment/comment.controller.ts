import {
    Controller,
    Get,
    Delete,
    UseGuards,
    Param,
    Logger,
    Post,
    Body,
    UsePipes,
    Query,
  } from '@nestjs/common';
  
  import { User } from '../user/user.decorator';
  import { AuthGuard } from '../share/auth.guard';
  import { ValidationPipe } from '../share/validation.pipe';
  import { CommentDTO } from './comment.dto';
  import { CommentService } from './comment.service';
  import { ApiTags, ApiNotFoundResponse, ApiForbiddenResponse, ApiCreatedResponse, ApiAcceptedResponse } from '@nestjs/swagger';
  
  @ApiTags('Comment controller')
  @Controller('api/comments')
  export class CommentController
  {
      constructor(private commentService: CommentService) { }
      
      @Get('card/:id')
      @ApiAcceptedResponse({description: 'Request accepted'})
      @ApiNotFoundResponse({description: 'Card not found'})
      getCardComments(@Param('id') card: string, @Query('page') page: number)
      {
          return this.commentService.getCardComments(card, page);
      }
      
      @Get('user/:id')
      @ApiAcceptedResponse({description: 'Request accepted'})
      @ApiNotFoundResponse({description: 'User not found'})
      getUserComments(@Param('id') user: string, @Query('page') page: number) 
      {
          return this.commentService.getUserComments(user, page);
      }

      @Get(':id')
      @ApiAcceptedResponse({description: 'Request accepted'})
      @ApiNotFoundResponse({description: 'Comment not found'})
      getComment(@Param('id') id: string) 
      {
          return this.commentService.get(id);
      }
      
      @Post('card/:id')
      @ApiCreatedResponse({description: 'Comment created'})
      @ApiForbiddenResponse({description: 'Cannot create comment'})
      @UseGuards(new AuthGuard())
      @UsePipes(new ValidationPipe())
      addComment(@Param('id') card: string, @User('id') user: string, @Body() data: CommentDTO)
      {
          return this.commentService.add(card, user, data);
      }
      
      @Delete(':id')
      @ApiAcceptedResponse({description: 'Comment deleted'})
      @ApiForbiddenResponse({description: 'Cannot delete comment'})
      @ApiNotFoundResponse({description: 'Comment not found'})
      @UseGuards(new AuthGuard())
      removeComment(@Param('id') id: string, @User('id') user: string)
      {
          return this.commentService.remove(id, user);
      }
  }