import {
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    Column,
    ManyToOne,
    JoinTable
} from 'typeorm';
  
import { UserEntity } from '../user/user.entity';
import { CardEntity } from '../card/card.entity';

@Entity('comment')
export class CommentEntity
{
    @PrimaryGeneratedColumn('uuid')
    id: string;
    
    @Column('text')
    comment: string;
    
    @ManyToOne(type => UserEntity)
    @JoinTable()
    owner: UserEntity;
    
    @ManyToOne(type => CardEntity, card => card.comms)
    card: CardEntity;
}