import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CardEntity } from '../card/card.entity';
import { UserEntity } from '../user/user.entity';
import { CommentEntity } from './comment.entity';
import { CommentDTO } from './comment.dto';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(CommentEntity)
    private commentRepo: Repository<CommentEntity>,
    @InjectRepository(CardEntity)
    private cardRepo: Repository<CardEntity>,
    @InjectRepository(UserEntity)
    private userRepo: Repository<UserEntity>,
  ) { }

  private toResponseObject(comment: CommentEntity) {
    return {
      ...comment,
      owner: comment.owner && comment.owner.toResponseObject(),
    };
  }
  
  private checkOwnership(comment: CommentEntity, userId: string)
  {
    if (comment.owner.id !== userId) {
        throw new HttpException(
          'Not an owner',
          HttpStatus.UNAUTHORIZED,
        );
      }
  }

  async getCardComments(cardId: string, page: number = 1) {
    const comments = await this.commentRepo.find({
      where: { card: { id: cardId } },
      relations: ['owner', 'card'],
      take: 25,
      skip: 25 * (page - 1),
    });
    return comments.map(comment => this.toResponseObject(comment));
  }

  async getUserComments(userId: string, page: number = 1) {
    const comments = await this.commentRepo.find({
      where: { author: { id: userId } },
      relations: ['owner', 'card'],
      take: 25,
      skip: 25 * (page - 1),
    });
    return comments.map(comment => this.toResponseObject(comment));
  }

  async get(id: string) {
    const comment = await this.commentRepo.findOne({
      where: { id },
      relations: ['owner', 'card'],
    });
    return this.toResponseObject(comment);
  }

  async add(cardId: string, userId: string, data: CommentDTO) {
    const card = await this.cardRepo.findOne({ where: { id: cardId } });
    const user = await this.userRepo.findOne({ where: { id: userId } });
    const comment = await this.commentRepo.create({
      ...data,
      card,
      owner: user,
    });
    await this.commentRepo.save(comment);
    return this.toResponseObject(comment);
  }

  async remove(id: string, userId: string) {
    const comment = await this.commentRepo.findOne({
      where: { id },
      relations: ['owner', 'card'],
    });
    this.checkOwnership(comment, userId);
    await this.commentRepo.remove(comment);
    return this.toResponseObject(comment);
  }
}