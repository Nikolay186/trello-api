import { Entity, PrimaryGeneratedColumn, Column,
BeforeInsert, OneToMany } from "typeorm";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import { UserRO } from "./user.dto";
import { CardEntity } from "src/card/card.entity";

@Entity('user')
export class UserEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;
    
    @Column({
        type: 'text',
        unique: true,
    })
    email: string;
    
    @Column({
        type: 'text',
        unique: true,
    })
    username: string;
    
    @Column('text')
    password: string;
    
    @OneToMany(type => CardEntity, card => card.owner, {cascade: true})
    cards: CardEntity[];
    
    @BeforeInsert()
    async hashPassword() 
    {
        this.password = await bcrypt.hash(this.password, 5);
    }
    
    async checkPassword(pwd: string): Promise<boolean>
    {
        return await bcrypt.compare(pwd, this.password);
    }
    
    toResponseObject(showToken: boolean = true): UserRO
    {
        const { id, username, email, token, password } = this;
        const responseObject: UserRO = {
            id,
            email,
            username,
            password
        };
        
        if (this.cards) {
            responseObject.cards = this.cards;
        }
        
        if (showToken) {
            responseObject.token = token;
        }
        return responseObject;
    }
    
    private get token(): string 
    {
        const { id, username } = this;
        return jwt.sign(
            {
                id,
                username
            },
            '123412312'
        );
    }
}