import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from "class-validator";
import { CardEntity } from "src/card/card.entity";

export class UserDTO {
    @ApiProperty()
    @IsNotEmpty()
    username: string;
    @ApiProperty()
    @IsEmail()
    email?: string;
    @ApiProperty()
    @IsNotEmpty()
    password: string;
    newPassword?: string;
}

export class UserRO {
    id: string;
    username: string;
    email?: string;
    token?: string;
    password?: string;
    cards?: CardEntity[];
}