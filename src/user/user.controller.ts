import { Controller, Get, Post, UsePipes, Body, Query, Param, Put, UseGuards } from "@nestjs/common";
import { UserService } from "./user.service"
import { User } from "./user.decorator";
import { UserDTO } from "./user.dto"
import { AuthGuard } from "../share/auth.guard"
import { ValidationPipe } from "../share/validation.pipe"
import { ApiTags, ApiNotFoundResponse, ApiForbiddenResponse, ApiAcceptedResponse, ApiCreatedResponse, ApiBadRequestResponse } from "@nestjs/swagger";

@ApiTags('User controller')
@Controller('api')
export class UserController 
{
    constructor(private userService: UserService) { }
    
    @Get('users')
    @ApiAcceptedResponse({description: 'Request accepted'})
    getAllUsers(@Query('page') page: number)
    {
        return this.userService.getAll(page);
    }
    
    @Get('users/:usrname')
    @ApiAcceptedResponse({description: 'Request accepted'})
    @ApiNotFoundResponse({description: 'User not found'})
    getUser(@Param('usrname') usrname: string)
    {
        return this.userService.getUser(usrname);
    }
    
    @Put('users/:id')
    @ApiAcceptedResponse({description: 'User info changed'})
    @ApiForbiddenResponse({description: 'Cannot change user info'})
    @ApiNotFoundResponse({description: 'User not found'})
    @UseGuards(new AuthGuard())
    editUser(@Param('id') id: string, @Body() data: UserDTO)
    {
        return this.userService.editUser(id, data);
    }
    
    @Get('auth/whoami')
    @ApiAcceptedResponse({description: 'Request accepted'})
    @ApiForbiddenResponse({description: 'Cannot get user info'})
    @UseGuards(new AuthGuard())
    getMe(@User('usrname') usrname: string)
    {
        return this.userService.getUser(usrname);
    }
    
    @Post('auth/register')
    @ApiCreatedResponse({description: 'User created'})
    @ApiBadRequestResponse({description: 'Cannot create user'})
    @UsePipes(new ValidationPipe())
    createUser(@Body() data: UserDTO)
    {
        return this.userService.register(data);
    }
    
    @Post('auth/login')
    @ApiAcceptedResponse({description: 'User logged in successfully'})
    @ApiBadRequestResponse({description: 'Cannot log in'})
    @UsePipes(new ValidationPipe())
    login(@Body() data: UserDTO)
    {
        return this.userService.login(data);
    }
    
    @Put('auth/changepass/:id')
    @ApiAcceptedResponse({description: 'Password changed'})
    @ApiForbiddenResponse({description: 'Cannot change password'})
    @ApiNotFoundResponse({description: 'User not found'})
    @UseGuards(new AuthGuard())
    updatePassword(@Param('id') id: string, @Body() data: UserDTO)
    {
        return this.userService.changePassword(id, data);
    }
}