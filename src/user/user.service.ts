import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UserEntity } from './user.entity';
import { UserDTO } from './user.dto';
import { UserRO } from './user.dto';


@Injectable()
export class UserService
{
    constructor(
        @InjectRepository(UserEntity)
        private userRepository: Repository<UserEntity>,
    ) { }
    
    async getAll(page: number = 1) 
    {
        const users = await this.userRepository.find({
            take: 25,
            skip: 25 * (page - 1)
        });
        return users.map(user => user.toResponseObject(false));
    }
    
    async getUser(usrname: string)
    {
        const user = await this.userRepository.findOne({
            where: { usrname }
        });
        return user.toResponseObject(false);
    }
    
    async editUser(id: string, data: Partial<UserDTO>): Promise<UserRO>
    {
        let user = await this.userRepository.findOne({where: { id }});
        if (!user) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        
        let { email, username } = data;
        let emailCheck = await this.userRepository.findOne({where: {email}});
        if (emailCheck && emailCheck.id !== id) {
            throw new HttpException('This email is already in use', HttpStatus.NOT_FOUND);
        }
        
        let nameCheck = await this.userRepository.findOne({where: {username}});
        if (nameCheck && nameCheck.id !== id) {
            throw new HttpException('This nickname is already in use', HttpStatus.NOT_FOUND);
        }
        
        await this.userRepository.update({id}, data);
        return user.toResponseObject();
    }
    
    async login(data: UserDTO)
    {
        const { username, password } = data;
        const userCheck = await this.userRepository.findOne({where: {username}});
        
        if (!userCheck || !(await userCheck.checkPassword(password))) {
            throw new HttpException(
                'Invalid username or password',
                HttpStatus.BAD_REQUEST
            );
        }
        return userCheck.toResponseObject();
    }
    
    async register(data: UserDTO)
    {
        const { username } = data;
        let userCheck = await this.userRepository.findOne({where: { username }});
        if (userCheck) {
            throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
        }
        let user = await this.userRepository.create(data);
        await this.userRepository.save(user);
        return user.toResponseObject();
    }
    
    async changePassword(id: string, data: UserDTO) 
    {
        const { password, newPassword } = data;
        
        const userCheck = await this.userRepository.findOne({where: {id}});
        if (!userCheck || !(await userCheck.checkPassword(password))) {
            throw new HttpException('Invalid password', HttpStatus.BAD_REQUEST);
        }
        let newPwd = await bcrypt.hash(newPassword, 10);
        await this.userRepository.update({id}, {password: newPwd});
    }
}