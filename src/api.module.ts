import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { CommentModule } from './comment/comment.module';
import { CardModule } from './card/card.module';
import { ColumnModule } from './column/column.module';

@Module({
    imports: [
        UserModule,
        CommentModule,
        ColumnModule,
        CardModule
    ],
    exports: [UserModule, CommentModule, ColumnModule, CardModule],
    controllers: [],
})
export class ApiModule { }